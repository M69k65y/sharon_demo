from datetime import datetime

from routes import db
from routes import json_cache
from database.image import Image
from database.shared import SharedColumns


class User(db.Model, SharedColumns):
    """
    A model representing the user database table
    """
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key = True)
    user_public_id = db.Column(db.String(100), unique = True, nullable = False, index = True)
    user_first_name = db.Column(db.String(100))
    user_last_name = db.Column(db.String(100))
    user_email = db.Column(db.String(100))
    user_phone = db.Column(db.String(100))
    # Relationships
    user_images = db.relationship("Image", backref = "rel_image_user", lazy = "dynamic")


    def __init__(self, user_public_id = None, user_first_name = None, user_last_name = None,
    user_email = None, user_phone = None):
        self.user_public_id = user_public_id
        self.user_first_name = user_first_name
        self.user_last_name = user_last_name
        self.user_email = user_email
        self.user_phone = user_phone


    def __repr__(self):
        return "<User {}, {}, {}>".format(self.user_public_id, self.user_first_name, self.user_last_name)


    def __str__(self):
        return "<User {}>".format(self.user_first_name)

        
    @json_cache.memoize()
    def return_json(self):
        # images = self.user_images.filter(Image.image_caption != None)\
        #                          .all()
        
        return {
            "user_public_id": self.user_public_id,
            "user_first_name": self.user_first_name,
            "user_last_name": self.user_last_name,
            "user_email": self.user_email,
            "user_phone": self.user_phone,
            "created_at": self.created_at,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "images": [
                image.return_json() for image in self.user_images
            ]
        }

    
    def return_name(self):
        return "Halt, my name is {}".format(self.user_first_name)