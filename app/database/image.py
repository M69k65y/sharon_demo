from datetime import datetime

from routes import db
from database.shared import SharedColumns


class Image(db.Model, SharedColumns):
	__tablename__ = "images"
	image_id = db.Column(db.Integer, primary_key = True)
	image_public_id = db.Column(db.String(100), index = True, nullable = False, unique = True)
	user_id = db.Column(db.String(100), db.ForeignKey("users.user_public_id"), index = True, nullable = False)
	image_url = db.Column(db.String(255), nullable = False)
	image_caption = db.Column(db.Text)


	def __init__(self, image_public_id = None, user_id = None,
	image_url = None, image_caption = None):
		self.image_public_id = image_public_id
		self.user_id = user_id
		self.image_url = image_url
		self.image_caption = image_caption

	
	def __repr__(self):
		return "<Image {}, {}, {}>".format(self.image_public_id, self.image_url, self.image_caption)

	
	def __str__(self):
		return "<Image {}>".format(self.image_url)


	def return_json(self):
		return {
			"image_public_id": self.image_public_id,
			"user_id": self.user_id,
			"image_url": self.image_url,
			"image_caption": self.image_caption,
			"created_at": self.created_at,
            "updated_at": self.updated_at,
			"user": self.rel_image_user.user_first_name + " " + self.rel_image_user.user_last_name
		}