from routes import db
from routes import json_cache

class SharedColumns():
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)


    def return_name(self):
        return "Lorem Ipsum"


    def _clear_cache(self):
        json_cache.delete_memoized(self.return_json, self)