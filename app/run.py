# import cherrypy

from routes import app


if __name__ == '__main__':
    # # Mount the application
    # cherrypy.tree.graft(app, "/")

    # # Unsubscribe the default server
    # cherrypy.server.unsubscribe()

    # # Instantiate a new server object
    # server = cherrypy._cpserver.Server()

    # # Configure the server object
    # server.socket_host = "0.0.0.0"
    # server.socket_port = 1010

    # # server.ssl_module            = 'builtin'
    # # server.ssl_certificate       = 'fullchain.pem'
    # # server.ssl_private_key       = 'privkey.pem'

    # # Subscribe this server
    # server.subscribe()

    # cherrypy.engine.start()
    # cherrypy.engine.block()

    ## DO NOT USE IN PRODUCTION
    app.run(port=1010, host='0.0.0.0', debug=True)