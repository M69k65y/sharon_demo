import requests

from flask import jsonify, request
from datetime import datetime

from routes import app, db
from routes import def_cache
from routes import auth_wrapper


@app.route("/", methods = ["GET"])
@app.route("/index", methods = ["GET", "PUT", "PATCH", "POST"])
@auth_wrapper()
def index_route():
	"""
	This is the index endpoint
	---
	tags:
		- base_urls.py
	responses:
		200:
			description:
				Everything is running smoothly. We are good to go.
			schema:
				properties:
					message:
						type: object
						example:
							["Hello world. My name is Sharon. GET"]
	"""
	message = []
	
	if request.method == "GET":
		message.append("Hello world. My name is Sharon. GET")
	elif request.method == "POST":
		message.append("Hello world. My name is Sharon. POST")
	elif request.method == "PUT":
		message.append("Hello world. My name is Sharon. PUT")
	elif request.method == "PATCH":
		message.append("Hello world. My name is Sharon. PATCH")
	else:
		message.append("Hello world. My name is Sharon. Default")

	return jsonify({
		"message": message
	}), 200


@app.route("/pass-info/<name>", methods = ["GET"])
def returning_info(name):
	"""
	Return a message with a name passed in
	---
	tags:
		- base_urls.py
		- testing_functions
	parameters:
		-	in: path
			name: name
			type: string
			example: John Doe
			description: The name of whoever
			required: true
	resonses:
		200:
			description:
				Super
			schema:
				properties:
					message:
						type: object
						example:
							["My name is John Doe"]
	"""
	response = {
		"message": "My name is {}".format(name)
	}

	return jsonify(response), 200


@app.route("/cache-test", methods = ["POST"])
@def_cache.cached(timeout = 3600)
def testing_cache():
	name = request.json.get("name")

	print(name)

	return jsonify({
		"message": "My name is {}".format(name)
	}), 200


@app.route("/memoize-test/<var_a>/<var_b>", methods = ["POST"])
@def_cache.memoize()
def testing_memo(var_a, var_b):
	int_a = int(var_a)
	int_b = int(var_b)
	# int_b = int(request.json.get("var_b"))

	print(int_a)
	print(int_b)
	
	return jsonify({
		"data": int_a * int_b
	}), 200