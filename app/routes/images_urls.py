import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from routes import app, db
from database.image import Image
from database.user import User


@app.route("/image/new", methods = ["POST"])
def save_image():
	"""
	Add image details
	"""
	image = Image(
		image_public_id = str(uuid.uuid4()),
		user_id = request.json.get("user_public_id"),
		image_url = request.json.get("image_url"),
		image_caption = request.json.get("image_caption")
	)

	db.session.add(image)

	try:
		db.session.commit()
		db.session.close()

		message = ["Saved image."]
		return jsonify({
			"message": message
		}), 201
	except Exception as e:
		db.session.rollback()
		db.session.close()

		message = ["Unable to save details"]
		return jsonify({
			"message": message,
			"exception": str(e)
		}), 406


@app.route("/images/all", methods = ["GET"])
def query_all_images():
	"""
	Return a list of all images
	"""
	try:
		page = int(request.args["page"])
	except ValueError:
		page = 0
	except KeyError:
		page = 1

	try:
		items = int(request.args["items"])
	except Exception:
		items = 5
	
	if page == 0:
		images = db.session.query(Image)\
						   .all()
	else:
		images = db.session.query(Image)\
						   .paginate(page, items, False)\
						   .items

	# images = db.session.query(Image)\
	# 				   .join(User, Image.user_id == User.user_public_id)\
	# 				   .add_columns(User.user_first_name, User.user_last_name,\
	# 					   			Image.image_public_id, Image.image_url, Image.image_caption)\
	# 				   .all()

	if not images:
		message = ["No images to display"]

		return jsonify({
			"message": message
		}), 412
	else:
		data = []

		for single in images:
			data.append(single.return_json())
			# return_data = {}
			# return_data["user_first_name"] = single.user_first_name
			# return_data["user_last_name"] = single.user_last_name
			# return_data["image_public_id"] = single.image_public_id
			# return_data["image_url"] = single.image_url
			# return_data["image_caption"] = single.image_caption

			# data.append(return_data)

		return jsonify({
			"data": data
		}), 200