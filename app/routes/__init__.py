# Only necessary when using SQLite
import sqlite3
# Only necessary when using MySQL
import pymysql

from flask import Flask, json, jsonify, request, redirect
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
from flasgger import Swagger
from datetime import datetime
from functools import wraps


app = Flask(__name__)
CORS(app)

json_cache = Cache(app, config = {
	"CACHE_TYPE": "filesystem",
	"CACHE_DIR": "cache/json",
	"CACHE_DEFAULT_TIMEOUT": 86400
})

def_cache = Cache(app, config = {
    "CACHE_TYPE": "simple"
})

# Only necessary for MySQL
pymysql.install_as_MySQLdb()

# MySQL
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://user:password@localhost/sharon_demo"
# PostgreSQL
# app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://user:password@localhost/sharon_demo"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] =  False
app.config["JSON_SORT_KEYS"] = False
app.config["JSONIFY_PRETTYPRINT_REGULAR"] = False

db = SQLAlchemy(app)

swagger = Swagger(app)

@app.after_request
def append_timestamp_to_json(response):
	data = response.json

	if data:
		access_time = datetime.now()

		data["access_time"] = access_time

		response.set_data(json.dumps(data))

	else:
		pass

	return response


## Wrapper
def auth_wrapper():
    def check_headers(func):
        @wraps(func)
        def wrapper_execute(*args, **kwargs):
            try:
                auth = request.headers["Authorization"]
            except ValueError:
                auth = None

            print(auth)
            
            if auth:
                return func(*args, **kwargs)
            else:
                return jsonify({
                    "message": "You do not have the necessary permissions to view this page"
                })
        
        return wrapper_execute
    
    return check_headers



from routes import base_urls
from routes import users_urls
from routes import images_urls