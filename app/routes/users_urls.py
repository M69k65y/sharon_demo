import requests
import uuid

from flask import jsonify, request
from datetime import datetime

from routes import app, db
from database.user import User
from database.image import Image


@app.route("/user/save", methods = ["POST"])
def add_new_user():
	"""
	Creating a new user
	---
	tags:
		- users_urls.py
	parameters:
		-	in: body
			name: body
			description: JSON parameters
			schema:
				properties:
					first_name:
						type: string
						description: The first name of a user
						example: John
						required: true
					last_name:
						type: string
						description: The last name of a user
						example: Doe
						required: true
					email:
						type: string
						description: Email address foo'
						example: test@mail.com
						required: true
	responses:
		201:
			description:
				User details saved
			schema:
				properties:
					message:
						type: object
						example:
							["User details saved"]
		406:
			description:
				Error saving details
			schema:
				properties:
					message:
						type: object
						example:
							["Unable to save the details for <User John>. Try again later."]
					exception:
						type: string
					object:
						type: string
						example: "<User John>"
	"""
	user_id = str(uuid.uuid4())
	
	user = User(
		user_public_id = user_id,
		user_first_name = request.json.get("first_name"),
		user_last_name =request.json.get("last_name"),
		user_email = request.json.get("email")
	)

	db.session.add(user)

	if request.json.get("images"):
		for image in request.json.get("images"):
			image = Image(
				image_public_id = str(uuid.uuid4()),
				user_id = user_id,
				image_url = image.get("image_url"),
				image_caption = image.get("image_caption")
			)

			print(image)

			db.session.add(image)

	try:
		db.session.commit()
		db.session.close()

		message = []
		message.append("User details saved")
		return jsonify({
			"message": message
		}), 201
	except Exception as error:
		db.session.rollback()
		db.session.close()

		# print(user)

		message = []
		message.append("Unable to save the details for {}. Try again later.".format(str(user)))
		return jsonify({
			"message": message,
			"exception": str(error),
			"object": str(user)
		}), 406


@app.route("/users/clear-cache", methods = ["GET"])
def clear_users_cache():
	"""
	
	"""
	users = db.session.query(User)\
					  .order_by(User.user_id.asc())\
					  .all()

	if not users:
		message = []
		message.append("No cached data to clear")
		
		return jsonify({
			"message": message
		}), 412
	
	else:
		for single in users:
			print(single.return_name())
			single._clear_cache()

		return jsonify({
			"message": ["Cache cleared"]
		}), 200


@app.route("/users/all", methods = ["GET"])
def query_all_users():
	"""
	Return a list of all users
	"""
	users = db.session.query(User)\
					  .order_by(User.user_id.asc())\
					  .all()

	if not users:
		message = []
		message.append("No users to display")
		
		return jsonify({
			"message": message
		}), 412
	
	else:
		# Master data
		data = []

		for single in users:
			# Single user data
			# return_data = {}
			# return_data["user_public_id"] = single.user_public_id
			# return_data["user_first_name"] = single.user_first_name
			# return_data["user_last_name"] = single.user_last_name
			# return_data["user_email"] = single.user_email

			# data.append(return_data)

			data.append(single.return_json())

		return jsonify({
			"data": data
		}), 200


@app.route("/user/<public_id>", methods = ["GET"])
def query_single_user(public_id):
	"""
	Return details of a single user
	"""
	user = db.session.query(User)\
					 .filter(User.user_public_id == public_id)\
					 .first()

	if not user:
		message = []
		message.append("The selected user does not appear to exist")

		return jsonify({
			"message": message
		}), 404
	else:
		data = [user.return_json()]

		return jsonify({
			"data": data
		}), 200