import os
import unittest
import sqlite3

from flask_sqlalchemy import sqlalchemy

from routes import app, db


class Test(unittest.TestCase):
	############################
	#### Setup and Teardown ####
	############################
	def setUp(self):
		app.config["TESTING"] = True
		app.config["DEBUG"] = False
		app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"
		self.app = app.test_client()
		# db.drop_all()
		db.create_all()

	
	def tearDown(self):
		# db.drop_all()
		pass

	
	###########################
	########## Tests ##########
	###########################
	# Tests are named test_101, test_102, test_n in order to set the order in which they run
	# https://stackoverflow.com/questions/30286268/order-of-tests-in-python-unittest
	# https://stackoverflow.com/questions/5387299/python-unittest-testcase-execution-order
	def test_101(self):
		"""
		Test working of index page
		"""
		response = self.app.get("/")
		self.assertEqual(response.status_code, 200)

	
	def test_102(self):
		"""
		Test index with method that's not allowed
		"""
		response = self.app.delete("/index")
		self.assertNotEqual(response.status_code, 200)

	
	def test_103(self):
		"""
		Test data in response
		"""
		response = self.app.patch("/index")
		self.assertIn("message", response.json)

	
	def test_104(self):
		"""
		Saving a new user's details
		"""
		response = self.app.post("/user/save", json = {
			"first_name": "Professor",
			"last_name": "X",
			"email": "prof@mail.com"
		})
		self.assertEqual(response.status_code, 201)


if __name__ == "__main__":
	unittest.main()